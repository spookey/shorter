# shorter

This is some kind of URL-shorter.

Give it something that looks like an URL, and it will return some short link.

Opening that short link will redirect you there.

## Redirection

* Short links are protected by the ``robots.txt``.
* A ``nofollow`` attribute is added to the link on the redirection page.
* The redirection page adds some ``X-Robots-Tag: noindex, nofollow`` header.
* Known crawlers will receive a 403 (Forbidden) error page instead of
  the redirection page.
* Redirects are done via JavaScript and/or HTML meta tags to remove the
  referrer.

## Deployment

Currently it runs on some FreeBSD server.
I won't get too specific here, but keep this in mind:

* Configuration is done via environment variables.
  Have a look into the ``shorter/start/environment.py`` file
  (everything using ``getenv`` is configurable).
* Use ``gmake`` for the ``makefile``.

I am using mysql as database, the setup is somewhat specific..

* Create Database:
  * Use ``utf8mb4`` because obvious reasons.
  * Use ``utf8mb4_bin`` as collation, because the symbols used are case
    sensitive.

```sql
  CREATE SCHEMA `shorter`
    DEFAULT CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin;
```

* Create user and set permissions:
  * Choose a better password than this!

```sql
  CREATE USER `shorter`@`localhost` IDENTIFIED BY 'password';
  GRANT ALL PRIVILEGES ON `shorter`.* TO 'shorter'@'localhost';
```

* Check your settings:

```sql
  USE `shorter`;
  SELECT @@character_set_database, @@collation_database;
```

It should output ``utf8mb4`` and ``utf8mb4_bin``.

* Now create the ``short`` table:

```sql
  USE `shorter`;
  DROP TABLE IF EXISTS `short`;

  CREATE TABLE `short` (
    `prime` bigint NOT NULL AUTO_INCREMENT,
    `symbol` varchar(256) NOT NULL,
    `target` varchar(2048) NOT NULL,
    `delay` int NOT NULL,
    `active` boolean NOT NULL,
    `visited` int NOT NULL,
    `created` datetime NOT NULL,
    PRIMARY KEY (`prime`),
    UNIQUE KEY `symbol` (`symbol`),
    CONSTRAINT `ctx_active` CHECK (`active` IN (0,1))
  )
  ENGINE=InnoDB AUTO_INCREMENT=1
  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
```

* Have fun by creating some entries:

```sql
  USE `shorter`;

  INSERT INTO `short` VALUES
    (1,'fck','https://www.die-partei.de',6,1,0,'1970-01-01 00:00:01'),
    (2,'AfD','https://afdblocker.de',6,1,0,'1970-01-01 00:00:02');
```

* Create the virtual environment and make sure the ``PyMySQL`` requirement
  is available:
  * You should have ``openssl`` for that.

```sh
    gmake requirements
```

Use some ``DATABASE`` string similar to this for the application:

```txt
  mysql+pymysql://shorter:password@localhost/shorter?charset=utf8mb4
```
