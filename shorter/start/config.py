from os import path, urandom
from re import IGNORECASE
from re import compile as rx_compile

from shorter.start.environment import (
    APP_NAME,
    BLOCK_BASE,
    BLOCK_FILE,
    CSRF_STRICT,
    DATABASE,
    DATABASE_DEV,
    HTML_LANG,
    PAGINATION,
    SECRET_BASE,
    SECRET_FILE,
    TITLE,
)


def secret_key(base=SECRET_BASE, filename=SECRET_FILE):
    location = path.abspath(path.join(base, filename))
    if not path.exists(location):
        secret = urandom(512)
        with open(location, "wb") as handle:
            handle.write(secret)
        return secret
    with open(location, "rb") as handle:
        return handle.read()


def url_blocklist(*presets, base=BLOCK_BASE, filename=BLOCK_FILE):
    def _compile(regex):
        return rx_compile(regex, IGNORECASE)

    location = path.abspath(path.join(base, filename))
    result = [_compile(rx) for rx in presets]
    if path.exists(location):
        with open(location, "r", encoding="utf-8") as handle:
            for line in [ln.strip() for ln in handle.readlines()]:
                if line and not line.startswith("#"):
                    result.append(_compile(line))
    return result


class BaseConfig:  # pylint: disable=too-few-public-methods
    APP_NAME = APP_NAME
    DEBUG = False
    HTML_LANG = HTML_LANG
    PAGINATION = PAGINATION
    SECRET_KEY = secret_key()
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    TESTING = False
    TITLE = TITLE
    WTF_CSRF_ENABLED = True
    WTF_CSRF_SSL_STRICT = CSRF_STRICT


class DevelopmentConfig(BaseConfig):  # pylint: disable=too-few-public-methods
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = DATABASE_DEV


class TestingConfig(BaseConfig):  # pylint: disable=too-few-public-methods
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite://"
    WTF_CSRF_ENABLED = False


class ProductionConfig(BaseConfig):  # pylint: disable=too-few-public-methods
    SQLALCHEMY_DATABASE_URI = DATABASE
