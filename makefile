DEBUG		:=	1
ENVIRON		:=	development
FLASK		:=	application.py

_HOST		:=	::1
_PORT		:=	5000

CMD_GIT		:=	git
CMD_SYS_PY	:=	python3
CMD_NPM		:=	npm

DIR_VENV	:=	venv
CMD_PIP		:=	$(DIR_VENV)/bin/pip3
CMD_PY		:=	$(DIR_VENV)/bin/python3
CMD_FLASK	:=	$(DIR_VENV)/bin/flask
CMD_BLACK	:=	$(DIR_VENV)/bin/black
CMD_ISORT	:=	$(DIR_VENV)/bin/isort
CMD_PYLINT	:=	$(DIR_VENV)/bin/pylint
CMD_PYTEST	:=	$(DIR_VENV)/bin/pytest

DIR_NDMD	:=	node_modules

DIR_SHORTER	:=	shorter
DIR_TESTS	:=	tests

FLE_STYLE	:=	$(DIR_SHORTER)/static/style.css

.PHONY: help
help:
	@echo "make shorter"
	@echo "————————————"
	@echo
	@echo "venv             install virtualenv"
	@echo "requirements     install requirements into venv"
	@echo
	@echo "dependencies     install dependencies into node_modules"
	@echo "assets           create required assets"
	@echo
	@echo "lint             run pylint"
	@echo "sort             run isort"
	@echo "black            run black"
	@echo "test             run pytest"
	@echo "tcov, tcovh      run test coverage (html)"
	@echo
	@echo "clean            show files to clean"
	@echo "cleanup          clean files unknown to git"
	@echo
	@echo "run              run application"
	@echo "shell            launch a shell"
	@echo
	@echo "                 🍌"
	@echo


###
# plumbing

define _pip
	$(CMD_PIP) install $(1)
endef
define _venv
	$(CMD_SYS_PY) -m "venv" "$(DIR_VENV)"
	$(call _pip,-U pip setuptools wheel)
endef

$(DIR_VENV):
	$(call _venv,)

$(CMD_FLASK): $(DIR_VENV)
	$(call _pip,-r "requirements.txt")
$(CMD_BLACK) $(CMD_ISORT) $(CMD_PYLINT) $(CMD_PYTEST): $(DIR_VENV)
	$(call _pip,-r "requirements-dev.txt")

.PHONY: requirements
requirements: $(CMD_FLASK)
.PHONY: requirements-dev
requirements-dev: $(CMD_BLACK) $(CMD_ISORT) $(CMD_PYLINT) $(CMD_PYTEST)


###
# assets

define _npm
	$(CMD_NPM) $(1)
endef

$(DIR_NDMD):
	$(call _npm,install)

$(FLE_STYLE): $(DIR_NDMD)
	$(call _npm,run build)

.PHONY: dependencies
dependencies: $(DIR_NDMD)
.PHONY: assets
assets: $(FLE_STYLE)


###
# service

define PYLINT_MESSAGE_TEMPLATE
{C} {path}:{line}:{column} - {msg}
  ↪  {category} {module}.{obj} ({symbol} {msg_id})
endef
export PYLINT_MESSAGE_TEMPLATE

define _lint
	$(CMD_PYLINT) \
		--disable "C0111" \
		--disable "R0801" \
		--enable "useless-suppression" \
		--msg-template="$$PYLINT_MESSAGE_TEMPLATE" \
		--output-format="colorized" \
			$(1)
endef

.PHONY: lint lintt
lint: $(CMD_PYLINT)
	$(call _lint,"$(DIR_SHORTER)")
lintt: $(CMD_PYLINT)
	$(call _lint,"$(DIR_TESTS)")


define _sort
	$(CMD_ISORT) \
		--line-width=79 \
		--profile=black \
			$(1)
endef

.PHONY: sort sortt
sort: $(CMD_ISORT)
	$(call _sort,"$(DIR_SHORTER)")
sortt: $(CMD_ISORT)
	$(call _sort,"$(DIR_TESTS)")


define _black
	$(CMD_BLACK) \
		--line-length=79 \
		$(1)
endef

.PHONY: black
black: $(CMD_BLACK)
	$(call _black,$(DIR_SHORTER))

.PHONY: blackt
blackt: $(CMD_BLACK)
	$(call _black,$(DIR_TESTS))


define _test
	$(CMD_PYTEST) -vv -rw $(1) --durations=10 "$(DIR_TESTS)"
endef
define _tcov
	$(call _test,$(1) --cov="$(DIR_SHORTER)" --cov="$(DIR_TESTS)")
endef


.PHONY: test tcov tcovh
test: $(CMD_PYTEST)
	$(call _test,)
tcov: $(CMD_PYTEST)
	$(call _tcov,)
tcovh: $(CMD_PYTEST)
	$(call _tcov,--cov-report="html:htmlcov")

tcovh-open: tcovh
	$(CMD_PY) -m webbrowser -t "htmlcov/index.html"

###
# cleanup

define _gitclean
	$(CMD_GIT) clean \
		-e "*.py" \
		-e "*.sqlite" \
		-e ".env" \
		-e "secret.key" \
		-e "$(DIR_VENV)/" \
		-e "$(DIR_NDMD)/" \
		$(1)
endef

.PHONY: clean cleanup
clean:
	$(call _gitclean,-ndx)
cleanup:
	$(call _gitclean,-fdx)


###
# flask

define _flask
	FLASK_DEBUG="$(DEBUG)" \
	FLASK_ENV="$(ENVIRON)" \
	FLASK_APP="$(FLASK)" \
	LOG_LVL="debug" \
	$(CMD_FLASK) $(1)
endef

.PHONY: shell run
run: $(CMD_FLASK) $(FLE_STYLE)
	$(call _flask,run --host "$(_HOST)" --port "$(_PORT)")
shell: $(CMD_FLASK)
	$(call _flask,shell)
