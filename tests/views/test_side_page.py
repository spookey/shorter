from flask import url_for
from pytest import mark

ENDPOINT = "side.page"


@mark.usefixtures("session")
class TestSidePage:
    @staticmethod
    @mark.usefixtures("ctx_app")
    def test_url():
        assert url_for(ENDPOINT, name="test") == "/page/test"

    @staticmethod
    def test_basic_views(visitor):
        res_details = visitor(ENDPOINT, params={"name": "details"})
        assert "details" in res_details.text
        assert "Wer? Wie? Wo? Wann? Was?" in res_details.text
        assert "Verlinkung" in res_details.text
        assert "Bookmarklet" in res_details.text

        res_privacy = visitor(ENDPOINT, params={"name": "privacy"})
        assert "privacy" in res_privacy.text
        assert "Datenschutz" in res_privacy.text
        assert "Datenschutzerklärung" in res_privacy.text
        assert "Datenschutzbeauftragter" in res_privacy.text

        res_imprint = visitor(ENDPOINT, params={"name": "imprint"})
        assert "imprint" in res_imprint.text
        assert "Impressum" in res_imprint.text
        assert "Kontakt" in res_imprint.text

    @staticmethod
    def test_hidden(visitor):
        res = visitor(ENDPOINT, params={"name": "_base"}, code=404)
        assert "404" in res.text

    @staticmethod
    def test_not_found(visitor):
        res = visitor(ENDPOINT, params={"name": "test"}, code=404)
        assert "404" in res.text
