from flask import url_for
from pytest import mark

ENDPOINT = "plus.index"


@mark.usefixtures("session")
class TestPlusIndex:
    @staticmethod
    @mark.usefixtures("ctx_app")
    def test_url():
        assert url_for(ENDPOINT) == "/plus/"

    @staticmethod
    def test_basic_view(visitor):
        res = visitor(ENDPOINT)
        assert "plus" in res.text

        assert "Plus" in res.text
        assert "Tabelle" in res.text
        assert "Blockiert" in res.text
        assert "Suchen" in res.text
        assert "form" in res.text
