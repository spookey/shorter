import cssdiscard from 'postcss-discard-comments';
import cssprefixer from 'autoprefixer';
import path from 'path';
import postcss from 'rollup-plugin-postcss';
import purgecss from '@fullhuman/postcss-purgecss';
import { fileURLToPath } from 'url';

const F_NAME = fileURLToPath(import.meta.url);
const D_NAME = path.dirname(F_NAME)

const SOURCE = path.normalize(path.join(
  D_NAME, 'source',
));
const STATIC = path.normalize(path.join(
  D_NAME, 'shorter', 'static'
));
const TMPLTS = path.normalize(path.join(
  D_NAME, 'shorter', 'templates'
));


const view = {
  input: path.join(SOURCE, 'style.scss'),
  output: {
    file: path.join(STATIC, 'style.css'),
    format: 'system',
  },
  plugins: [
    postcss({
      plugins: [
        cssprefixer(),
        cssdiscard({
          removeAll: true,
        }),
        purgecss({
          content: [
            path.join(TMPLTS, '**', '*.html'),
            path.join(TMPLTS, '*.html'),
          ],
        }),
      ],
      extract: true,
      minimize: true,
      sourceMap: false,
      use: {
        sass: {
          quietDeps: true,
        },
      },
    }),
  ],
};


export default [
  view,
];
